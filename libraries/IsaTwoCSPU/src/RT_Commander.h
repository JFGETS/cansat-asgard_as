#pragma once
#include "Arduino.h"
#include "AcquisitionProcess.h"
#include "elapsedMillis.h"
#include "IsaTwoConfig.h"
#ifdef RF_ACTIVATE_API_MODE
#include "IsaTwoXBeeClient.h"
#endif

/** @ingroup cansatAsgardCSPU
    @brief This class is in charge of the Real-time communication with the ground.

    It receives commands and sends responses.
    Commands must be in CSV format, as described in IsaTwoInterface.h
    The class analyzes only the command which begin with the same number from theCommandTypeId
      the number after de first "," indicates the command's type c.f isaTwoInterface.h
      there are two mode the acquisition and the command mode
      -the command mode: the class read all the instruction and finished after the
       timeout define by the third parameter of the begin function
      -the acquisition mode: the class read only one instruction "go to the command
       mode"
*/


class RT_Commander {
  public:
    /** @brief The various states the can be be in. */
    enum class State_t {
      Acquisition,                /**< Acquisition mode state: the RT-Commander does not respond to commands except to switch to Command mode */
      Command                     /**< Command mode state: the RT-Commander is responding to commands. The RT-Commander switches back to Acquisition mode after a predefined timeout */
    };

    /** @brief Constructor of the class RT_Commander.
        @param theCommandTypeId The command type the class has to analyse
        @param theTimeOut The duration (in milliseconds) for the Command mode to time out (and switch back to acquisition).
    */
    RT_Commander( byte theCommmandTypeId, unsigned long int theTimeOut);

#ifdef RF_ACTIVATE_API_MODE
    /** @brief Method used to initialize the object when using the RF API mode.
         @param xbeeClient The interface to the XBee module to used for output .
         @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
         @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.
     */
     void begin(IsaTwoXBeeClient& xbeeClient, SdFat* theSd = NULL, AcquisitionProcess* theProcess = NULL);
#else
    /** @brief Method used to initialize various pointers.
        @param RF_Stream The output stream used by the class.
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type AcquisitionProcess used for interaction with it by the class when handling commands.
    */
    void begin(Stream& RF_Stream, SdFat* theSd = NULL, AcquisitionProcess* theProcess = NULL);
#endif

    /** @brief Method used to process a command.
        @param theMsg The message/command to analyze and possibly process.
    */
    void processMsg(const char* theMsg);

    /** @brief Method used to check if the Command mode has timed out.*/
    State_t currentState() {  checkTimeout();   return state; 	}

  protected:
    /** Process a request while in command mode.
     * @param requestType the request type value
     * @param cmd	Pointer to the first character after the command type. Command parameter can be parsed from
     * 				this position which should point to a separator if any parameter is present, or to the final '\0'
     * 				if none is provided.
     */
    void processRequest(int requestType, char* cmd);
    void processReq_DigitalWrite(char* &nextCharAddress);
	void processReq_ListFiles(char* &nextCharAddress);
	void processReq_GetFile(char* &nextCharAddress);
	void processReq_StartCampaign(char* &nextCharAddress);
	void processReq_StopCampaign(char* &nextCharAddress);

	/** Extract a mandatory integer parameter from a command.
	 *  @param nextCharAddress Pointer to the first character to parse. Should point to a separator if any parameter is present,
	 *  					   or to the final '\0'	if none is provided.
	 *  					   After parsing, this pointer is updated to the first unparsed character.
	 *  @param value The parsed value
	 *  @param errorMsg The error message to use if the required parameter cannot be found.
	 *  @return True if the parameter was succesfully parsed, false otherwise.
	 */
	bool getMandatoryParameter(char* &nextCharAddress, long int& value, const char* errorMsg);
	bool getMandatoryParameter(char* &nextCharAddress, byte& value, const char* errorMsg);
	bool getMandatoryParameter(char* &nextCharAddress, const char* &value, const char* errorMsg);

	/** Extract an optional integer parameter from a command.
	 *  @param nextCharAddress Pointer to the first character to parse. Should point to a separator if any parameter is present,
	 *  					   or to the final '\0'	if none is provided.
	 *  					   After parsing, this pointer is updated to the first unparsed character.
	 *  @param value The parsed value
	 *  @param defaultValue The value to use in case the parameter is not present or cannot
	 *  					be parsed.
	 *  @return True if the parameter was found and successfully parsed, false if the
	 *  			 it was absent or invalid and the default value was used.
	 */
	bool getParameter(char* &nextCharAddress, byte& value, byte defaultValue=0);
	bool getParameter(char* &nextCharAddress, long int& value, long int defaultValue=0L);
	bool getParameter(char* &nextCharAddress, uint32_t& value, uint32_t defaultValue);

	/** Extract an optional string parameter from a command.
	 *  @param nextCharAddress Pointer to the first character to parse. Should point to a separator if any parameter is present,
	 *  					   or to the final '\0'	if none is provided.
	 *  					   After parsing, this pointer is updated to the first unparsed character.
	 *  @param value (out) A pointer to the parsed value, or a pointer to the default value, if no
	 *  				   value provided in the command.
	 *  @param defaultValue The value to use in case the parameter is not present or cannot
	 *  					be parsed.
	 *  @return True if the parameter was found and successfully parsed, false if the
	 *  			 it was absent or invalid and the default value was used.
	 */
	bool getParameter(char* &nextCharAddress, const char*& value, const char* defaultValue="");

    /** @brief Methd used to check the time out to switch to Acquisition mode.*/
    void checkTimeout();

  private:
    SdFat* sd;                                /**< The sd object used for various sd related actions.*/
    byte commandTypeId; 		                  /**< The type of command to respond.*/
    unsigned long int cmdModeTimeOut; 	      /**< The time before switching to acquisition mode.*/
    elapsedMillis inactivityDuration; 		    /**< Time spent in command-mode without activity.*/
    State_t state; 				                    /**< The current state of the RT_Commander.*/
#ifdef RF_ACTIVATE_API_MODE
    IsaTwoXBeeClient* RF_Stream;				  /**< The pseudo stream used when the API mode is activated */
#else
    Stream* RF_Stream; 	                      /**< The stream used to output command responses (when using
     	 	 	 	 	 	 	 	 	 	 	   transparent mode */
#endif
    AcquisitionProcess* acqProcess;           /**< A pointer to the acquisition process, to interact with it, when handling commands.*/
};
