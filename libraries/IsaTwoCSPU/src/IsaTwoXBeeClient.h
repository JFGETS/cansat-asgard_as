/*
   IsaTwoXbeeClient.h
*/

#include "XBeeClient.h"
#include "IsaTwoInterface.h"

#pragma once

#define XBEECLIENT_INCLUDES_UTILITIES

class IsaTwoXBeeClient : public XBeeClient {
  public:
    IsaTwoXBeeClient(uint32_t SH, uint32_t SL): XBeeClient(SH, SL) {};
    virtual ~IsaTwoXBeeClient() {};
    /**Send a data record.
       @param record, The payload to transfer
       @param timeOutCheckResponse The maximum delay to wait for a reception
        				acknowledgment by the destination XBee (msec). If 0,
        				the reception ack will not be checked.

       @return True if transfer is successful, false otherwise
    */
    virtual bool send(const IsaTwoRecord& record,  int timeOutCheckResponse = 0);

    /** Begin a new string message of the provided type.
       @param recordType The record type set in the frame's first byte.
    */
    virtual void openStringMessage(IsaTwoRecordType recordType = IsaTwoRecordType::StatusMsg);

#ifdef XBEECLIENT_INCLUDES_UTILITIES
    /** This version just intercept calls to superclass' send(), in order to display
        debug information.
    */
    virtual bool send(	uint8_t* data, uint8_t dataSize,
                        int timeOutCheckResponse = 300,
                        uint32_t SH = 0, uint32_t SL = 0);
#endif

    /** Extract data from the payload to fill an IsaTwoRecord.
       @param record The record to fill with the data
       &param data Payload pointer
       @param dataSize Payload size
       @pre The payload is expected to be exactly a byte with value
      		IsaTwoRecordType::DataRecord, an unused byte and
            an IsaTwoRecord.
       @return True if operation was successful, false otherwise.
    */
    bool getDataRecord(IsaTwoRecord& record, const uint8_t* data, uint8_t dataSize);
    
    /** Extract data from the payload to fill an string buffer.
	* @param data The buffer to fill with the null-terminated string.
	* 			 allocated by the caller, must be at least dataSize bytes.
	* &param data Payload pointer
	* @param dataSize Payload size
	* @pre The payload is expected to be exactly a byte with value
	* 		IsaTwoRecordType::DataRecord followed by an unused byte
	* 		and a null-terminated string..
	* @return True if operation was successful, false otherwise.
	*/
   	bool getString(char* string, const uint8_t* data,uint8_t dataSize);

#ifdef XBEECLIENT_INCLUDES_UTILITIES
    void displayFrame(uint8_t* data, uint8_t dataSize);
#endif
  protected:

    static constexpr bool displayOutgoingFramesOnSerial = false;
};
