/*
 * cansatAsgardCSPU.h 
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup cansatAsgardCSPU 
 * in the class documentation block.
 */

 /** @defgroup cansatAsgardCSPU cansatAsgardCSPU library 
 *  @brief The library of general purpose classes, relevant to any Cansat or Asgard project.
 *  
 *  The Cansat-Asgard library contains various high-level classes without any specific link to a particular project:
 *  - Interface to sensors (without enforcing project-specific configuration)
 *  - Generic classes to manage the hardware
 *  - Generic classes to implement an AHRS system
 *  - Generic classes to support the on-board processing cycle 
 *  - ....
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  
 *  
 *  _History_\n
 *  The library was created by the 2017-18 Cansat team (ISATIS) and further enriched
 *  during the next projects. 
 */

