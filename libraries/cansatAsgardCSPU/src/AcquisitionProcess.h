/*
    AcquisitionProcess.h
    Created on: 19 janv. 2018

*/

#pragma once
#include "StorageManager.h"

#include "elapsedMillis.h"

/** @ingroup cansatAsgardCSPU 
 *  @brief An abstract class implementing all the logic common to any acquisition process:
    detect campaign start condition, read data from sensors, and store using a provided StorageManager
 */
class AcquisitionProcess {
  public:
    /* Constants. Could not name them just On and Off because of a number
     * of #defines in headers (e.g. ArduCAM.h) */
    static const bool AcqOn = true; /**< Constant for switching an element on */
    static const bool AcqOff = false; /**< Constant for switching an element off */

    /** 
     @param acquisitionPeriodInMsec The period of the acquisition cycle, in msec 
     */
    AcquisitionProcess(unsigned int acquisitionPeriodInMsec);
    virtual ~AcquisitionProcess() {};

    /** This is the method to call in the main loop() to have acquisition cycles performed 
     *  with the period defined by the last call to setMinDelayBetweenAcquisition().
     *  @pre The #init() method has been called */
    NON_VIRTUAL void run();

    /** Call once and only once before calling run(). 
     *  getHardwareScanner() is expected to return an initialized HardwareScanner when this method is called. 
     */
    VIRTUAL void init();

    /** Implement in subclass to make the appropriate HardwareScanner available to the process 
    */
    VIRTUAL HardwareScanner* getHardwareScanner() = 0;
    /** Obtain a pointer to the SdFat object (fully initialized and ready to use) if any
     *  This version returns NULL and must be implemented by subclasses if relevant.
     *  @return Pointer to the SdFat object, or null if none available.
     */
    VIRTUAL SdFat* getSdFat() { return NULL;};

    /** Force the start of the measurement campaign. This method is empty and should be
     *  overloaded by subclasses which implement a campaign start condition.
     *  @param msg A message to send to document the reason of the start campaign.
     *  @param value An optional numeric value to document the reason of the start campaign
     */
    VIRTUAL void startMeasurementCampaign(const char* msg, float value=0.0f) {};
    /** Force the interruption of the measurement campaign. This method is empty and should be
     *  overloaded by subclasses which implement a campaign start condition.
     */
    VIRTUAL void stopMeasurementCampaign() {};
  protected:
    /** Implement in subclass to store a data record.
     *  @param campaignStarted true if the campaign is started, false
     *         if the can is in pre-flight conditions.
     */
    VIRTUAL void storeDataRecord(const bool campaignStarted) = 0;
    
    /** Method called whenever the run() method is called. Implement to perform  
     *  housekeeping operations, if any. 
     *  Warning: this method is called very often: be sure not to perform housekeeping tasks
     *  everytime, if not strictly required. 
     */
    VIRTUAL void doIdle() {};
    
     /** Switch LEDs on or off. 
      *  @param type Identifies the LED to adress
      *  @param status  On or Off
      */
    NON_VIRTUAL void setLED(HardwareScanner::LED_Type type, bool status);

  private:
    /** Implement in your subclass to actually acquire the data from the sensors */
    VIRTUAL void acquireDataRecord() = 0;

    /** Define whether this data set is worth storing. This default implementation always returns true */
    VIRTUAL bool isRecordRelevant() const;

    /** Define whether the measurement campain is started
       This default implementation always returns true but should be overloaded in the subclasses to define 
       a real condition to detect when the payload actually measures relevant data.  */
    VIRTUAL bool measurementCampaignStarted() {
      return true;
    } ;

    /** Configure the various output pins used to connect the LEDs controlled by the AcquisitionProcess */
    NON_VIRTUAL void initLED_Hardware();

    /** Implement this method in your subclass to perform whatever project-specific initialisation is
     *  required.
     */
    virtual void initSpecificProject() {};

    /** Change the state of the Heart beat LED, if it is remained unchanged long enough. 
     *  This method is called whenever the run() method is called.
     */
    void blinkHeartbeatLED ();

    unsigned int periodInMsec; /**< The acquisition period, in milliseconds. */
    elapsedMillis elapsedTime; /**< The time elapsed since the previous cycle in milliseconds */
    elapsedMillis heartbeatTimer; /**< The time elapsed since the heart beat LED was last updated. */
    bool heartbeatLED_State;   /**< The current state of the heart beat LED. */ 

};
