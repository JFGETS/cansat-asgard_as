/*
    Test of EEPROM_BankWrite and EEPROM_Bank classes.

    IMPORTANT NOTE: Be sure to define symbol TESTS_0_TO_3 (and not TEST_4_TO_6) and then the opposite.
         Define TEST_PERFORMANCE to run performance test (but beware it costs several hundreds write cycles).
         The tests are too large to fit in a 32kb program memory all together.

    MISSING TEST: read a record accross two chips! TO ADD!
    Done: Test 4b is prepared (not tested). Uncomment in setup() when other tests succeed.

*/
#include "Arduino.h"
#include "TestHardwareScanner.h"
#include "ExtEEPROM.h"
#include "EEPROM_BankWithTools.h"
//#define DEBUG
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "utilityFunctions.h"
#include "TestHW_ScannerSmallChips.h"

#define KEY 0x1111

//#define TESTS_0_TO_3
#define TESTS_4_TO_6
#define TEST_PERFORMANCE // Do not run too often: consumes many write-cycles; 

#ifdef TESTS_0_TO_3

void test0_NoEEPROM_Chip() {
  Serial << ENDL << F("### Test 0 : no Chip reported by HW Scanner") << ENDL;
  TestHardwareScanner hw;
  hw.init(10, 100);
  EEPROM_BankWriter bank(1);
  bool success = bank.init(KEY, hw, sizeof(TestRecord)); // This should fail because no EEPROM
  DASSERT(!success);
  Serial << "OK" << ENDL;
}
void test1_EmptyEEPROM() {
  Serial << ENDL << F("### Test 1 Chips detected by HWScanner (run this with several HW configurations)") << ENDL;
  HardwareScanner hw;
  hw.init(10, 100);
  hw.printFullDiagnostic(Serial);
  if (hw.getNumExternalEEPROM() == 0) {
    Serial << F("** Error: No EEPROM on this board (required for testing) ! Aborted.") << ENDL;
    Serial.flush();
    exit(-1);
  }
  // Make sure there is no valid header in firstEEPROM.
  DASSERT(ExtEEPROM::writeByte(hw.getExternalEEPROM_I2C_Address(0), 0, 0x00));
  DASSERT(ExtEEPROM::writeByte(hw.getExternalEEPROM_I2C_Address(0), 1, 0x00));
  initTestData();
  EEPROM_BankWithTools bank(1);

  bool success = bank.init(KEY, hw, sizeof(TestRecord)); // The header should not be found and consequently written.
  bank.printHeaderFromChip();
  DASSERT(success);

  Serial << F("  A") << ENDL;
  bank.printHeaderFromChip();
  unsigned long expectedSize = getExpectedTotalSize(hw);
  unsigned long expectedFreeSpace = expectedSize - sizeof(EEPROM_Bank::EEPROM_Header);
  checkSpace(bank, expectedSize, expectedFreeSpace);


  Serial << F("  B1") << ENDL ;
  storeDataRecords(&bank, 1);

  Serial << F("  B2")  << ENDL;
  expectedFreeSpace -= (sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);

  Serial << F("  C")  << ENDL;
  storeDataRecords(&bank, 2);

  Serial << F("  D")  << ENDL;
  expectedFreeSpace -= (2 * sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);

  Serial << F("Simulate almost full first chip & store one more record, to exceed chip...") << ENDL;
  bank.eraseFrom(0, hw.getExternalEEPROM_LastAddress(0) - 5);
  Serial << F("  D1")  << ENDL;
  expectedFreeSpace =  hw.getNumExternalEEPROM() * (1L + hw.getExternalEEPROM_LastAddress(0))
                       - (hw.getExternalEEPROM_LastAddress(0) - 5);
  bank.printHeaderFromChip();
  Serial << F("  D2")  << ENDL;
  checkSpace(bank, expectedSize, expectedFreeSpace);
  storeDataRecords(&bank, 1);
  expectedFreeSpace -= (sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);
  storeDataRecords(&bank, 3);
  //bank->printMemory();
  expectedFreeSpace -= (3 * sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);
  doIdle(bank, 2);
  bank.printHeaderFromChip();

  Serial << "Deleting..." << ENDL;
  bank.erase();
  initTestData();
  bank.printHeaderFromChip();
  expectedSize = getExpectedTotalSize(hw);
  expectedFreeSpace = expectedSize - sizeof(EEPROM_Bank::EEPROM_Header);
  checkSpace(bank, expectedSize, expectedFreeSpace);

  testData.aByte = 0xAA;
  Serial << " Leaving EEPROM with valid header and 10 records to read." << ENDL;
  storeDataRecords(&bank, 10);
  //bank->printMemory();
  expectedFreeSpace -= 10 * sizeof(TestRecord);
  checkSpace(bank, expectedSize, expectedFreeSpace);
  doIdle(bank, 2);
  bank.printHeaderFromChip();

  Serial << ENDL;
}

void test2_EEPROM_NonEmptyWithHeader() {
  Serial << ENDL << F("### Test 2 Non empty header in first chip") << ENDL;
  Serial << F("We should find the header written by test 1.") << ENDL;
  HardwareScanner hw;
  hw.init(10, 100);
  EEPROM_BankWithTools bank(1);
  bool success = bank.init(KEY, hw, sizeof(TestRecord));
  DASSERT(success);

  bank.printHeaderFromChip();
  // The header should be found and used.
  unsigned long expectedSize = getExpectedTotalSize(hw);
  unsigned long expectedFreeSpace = expectedSize - sizeof(EEPROM_Bank::EEPROM_Header)
                                    - 10 * sizeof(TestRecord);
  checkSpace(bank, expectedSize, expectedFreeSpace);
  initTestData(0xff, 0xff - 10);

  DASSERT(bank.leftToRead() == 10 * sizeof(TestRecord));
  DASSERT(bank.recordsLeftToRead() == 10);

  storeDataRecords(&bank, 1);
  expectedFreeSpace -= (sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);
  DASSERT(bank.leftToRead() == 11 * sizeof(TestRecord));
  DASSERT(bank.recordsLeftToRead() == 11);
  DASSERT(!bank.readerAtEnd());


  Serial << F("Reading 11 records") << ENDL;
  readDataRecords(&bank, 11);
  DASSERT(bank.leftToRead() == 0);
  DASSERT(bank.recordsLeftToRead() == 0);
  DASSERT(bank.readerAtEnd());

  Serial << F("Resetting and reading again...") << ENDL;
  bank.resetReader();
  initTestData(0xff, 0xff - 11);
  DASSERT(bank.leftToRead() == 11 * sizeof(TestRecord));
  DASSERT(bank.recordsLeftToRead() == 11);
  DASSERT(!bank.readerAtEnd());

  Serial << F("Attempting to read past end...") << ENDL;
  readDataRecords(&bank, 11);
  DASSERT(bank.leftToRead() == 0);
  DASSERT(bank.recordsLeftToRead() == 0);
  DASSERT(bank.readerAtEnd());
  TestRecord data;
  success = bank.readOneRecord((byte*) &data, sizeof(data));
  DASSERT(!success);
  byte read = bank.readData((byte*) &data, 1);
  DASSERT(read == 0);
  Serial << F("Reading failed: OK.") << ENDL;
}

void test3_EEPROM_EmptyWithHeader() {
  Serial << ENDL << "### Test 3: empty EEPROM with header " << ENDL;
  HardwareScanner hw;
  hw.init(10, 100);
  // Clear data left from previous tests.
  EEPROM_BankWithTools bank0(1);
  bool success = bank0.init(KEY, hw, sizeof(TestRecord));
  DASSERT(success);

  bank0.erase();
  EEPROM_BankWithTools bank(1);
  success = bank.init(KEY, hw, sizeof(TestRecord));
  DASSERT(success);
  bank.printHeaderFromChip();
  // The header should be found and used.
  unsigned long expectedSize = getExpectedTotalSize(hw);
  unsigned long expectedFreeSpace = expectedSize - sizeof(EEPROM_Bank::EEPROM_Header);
  checkSpace(bank, expectedSize, expectedFreeSpace);
  initTestData(0xbb, 0xbb);

  DASSERT(bank.leftToRead() == 0);
  DASSERT(bank.recordsLeftToRead() == 0);

  storeDataRecords(&bank, 1);
  expectedFreeSpace -= (sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);
  DASSERT(bank.leftToRead() == 1 * sizeof(TestRecord));
  DASSERT(bank.recordsLeftToRead() == 1);
  DASSERT(!bank.readerAtEnd());


  Serial << F("Reading the 1 record") << ENDL;
  readDataRecords(&bank, 1);
  DASSERT(bank.leftToRead() == 0);
  DASSERT(bank.recordsLeftToRead() == 0);
  DASSERT(bank.readerAtEnd());
}

#endif

#ifdef TESTS_4_TO_6
void test4_EEPROM_MoreThanOneChipFull() {
  Serial << ENDL << "### Test 4 : chip 0 to n-1 full, last chip partly full" << ENDL;
  HardwareScanner hw;

  hw.init(10, 100);
  hw.printFullDiagnostic(Serial);
  // Clear data left from previous tests.
  DASSERT(hw.getNumExternalEEPROM() >= 3);

  unsigned int wrongKey = 0xBAAF;
  unsigned int rightKey = 0xACAF;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &wrongKey, sizeof(rightKey)));
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(2), 0, (byte *) &wrongKey, sizeof(rightKey)));
  EEPROM_Bank::EEPROM_Header h;
  h.headerKey = rightKey;
  h.chipLastAddress = hw.getExternalEEPROM_LastAddress(0);
  h.numChips = hw.getNumExternalEEPROM();
  h.recordSize = sizeof(TestRecord);
  h.firstFreeChip = hw.getNumExternalEEPROM()-1;
  h.firstFreeByte = h.chipLastAddress - 2 - h.recordSize; // Should accept one more record, not 2.
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(0), 0, (byte *) &h, sizeof(h)));
  initTestData(21, 21);

  EEPROM_BankWithTools bank(1);
  bool success = bank.init(rightKey, hw, sizeof(TestRecord));
  DASSERT(success);

  bank.printHeaderFromChip();
  // The header should be found and used.
  unsigned long expectedSize = getExpectedTotalSize(hw);
  unsigned long expectedFreeSpace =  h.recordSize + 2 + 1;
  checkSpace(bank, expectedSize, expectedFreeSpace);
  DASSERT(bank.getNumFreeRecords() == 1);
  Serial << F("Store last possible record...") << ENDL;
  storeDataRecords(&bank, 1);
  expectedFreeSpace -= (sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);
  DASSERT(bank.getNumFreeRecords() == 0);

  Serial << F("Store one more record, to exceed...") << ENDL;
  DASSERT(!bank.storeOneRecord((byte*) &testData, sizeof(testData)));
  checkSpace(bank, expectedSize, 0);
  DASSERT(bank.getNumFreeRecords() == 0);
  DASSERT(bank.memoryFull());
}

void test4b_WriteThenReadAccrossChips() {
  Serial << ENDL << "### Test 4b : Fill and reread more then 1 chip" << ENDL;
  TestHW_ScannerSmallChips hw; // This HW_Scanner reports chips with a very limited size, so we can easily used several chips.
  hw.init(10, 100);
  // Clear data left from previous tests.
  DASSERT(hw.getNumExternalEEPROM() >= 2);
  unsigned int wrongKey = 0xBAAF;
  unsigned int rightKey = 0xACAF;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(0), 0, (byte *) &wrongKey, sizeof(rightKey)));
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &wrongKey, sizeof(rightKey)));
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(2), 0, (byte *) &wrongKey, sizeof(rightKey)));
  initTestData(1 , 1);

  EEPROM_BankWithTools bank(1);
  bool success = bank.init(rightKey, hw, sizeof(TestRecord));
  DASSERT(success);

  bank.printHeaderFromChip();
  // The header should be initialised
  unsigned long expectedSize = getExpectedTotalSize(hw);
  unsigned long expectedFreeSpace = expectedSize - sizeof(EEPROM_Bank::EEPROM_Header);
  checkSpace(bank, expectedSize, expectedFreeSpace);
  unsigned long numRecords = bank.getNumFreeRecords();
  Serial << F("We should be able to store ") << numRecords  << F(" records: let's do it.") << ENDL;
  storeDataRecords(&bank, numRecords);
  expectedFreeSpace -= numRecords*(sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);
  DASSERT(bank.getNumFreeRecords() == 0);
  Serial << F("Store one more record, to exceed...") << ENDL;
  DASSERT(!bank.storeOneRecord((byte*) &testData, sizeof(testData)));
  checkSpace(bank, expectedSize, 0);
  DASSERT(bank.getNumFreeRecords() == 0);
  DASSERT(bank.memoryFull());

  // Now read all those records back
  readDataRecords(&bank,numRecords);
}

void test5_TwoHeaders() {
  Serial << ENDL << F("### Test 5: Two headers found (in chips 1-2)") << ENDL;
  HardwareScanner hw;
  hw.init(10, 100);
  hw.printFullDiagnostic(Serial);
  // Install header in EEPROM 2 & 3.
  DASSERT(hw.getNumExternalEEPROM() >= 2);
  unsigned int wrongKey = 0xBAAF;
  unsigned int rightKey = 0xAAAF;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(0), 0, (byte *) &wrongKey, sizeof(wrongKey)));

  // Lets write an acceptable header in chip 1 and the same in chip2: it should be accepted
  Serial << F("Should succeed...");
  EEPROM_Bank::EEPROM_Header h;
  h.headerKey = rightKey;
  h.chipLastAddress = hw.getExternalEEPROM_LastAddress(0);
  h.numChips = 2;
  h.recordSize = sizeof(TestRecord);
  h.firstFreeChip = 1;
  h.firstFreeByte = 0x0A;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &h, sizeof(h)));
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(2), 0, (byte *) &h, sizeof(h)));
  EEPROM_BankWithTools bank(1);
  DASSERT(bank.init(rightKey, hw, sizeof(TestRecord)));
  Serial << F("OK") << ENDL;

  // Idem with numChips = 3. This should be refused.
  h.numChips = 3;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &h, sizeof(h)));
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(2), 0, (byte *) &h, sizeof(h)));
  EEPROM_BankWithTools bank1(1);
  Serial << F("Should fail...");
  DASSERT(!bank1.init(rightKey, hw, sizeof(TestRecord)));

  Serial << F("OK") << ENDL << ENDL;
}

void test5b_InconsistentHeaders() {
  Serial << ENDL << F("### Test 5b: Inconsistent header") << ENDL;
  HardwareScanner hw;
  hw.init(10, 100);
  hw.printFullDiagnostic(Serial);
  // Install header in EEPROM 2 & 3.
  DASSERT(hw.getNumExternalEEPROM() >= 2);
  unsigned int wrongKey = 0xBAAF;
  unsigned int rightKey = 0xAAAF;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &wrongKey, sizeof(wrongKey)));
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(2), 0, (byte *) &wrongKey, sizeof(wrongKey)));
  EEPROM_Bank::EEPROM_Header h;
  h.headerKey = rightKey;
  h.chipLastAddress = hw.getExternalEEPROM_LastAddress(0);
  h.numChips = hw.getNumExternalEEPROM();

  // 1. wrong record size
  Serial << F("wrong record size...") << ENDL;
  h.recordSize = sizeof(TestRecord) + 1;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(0), 0, (byte *) &h, sizeof(h)));
  EEPROM_BankWithTools bank0(1);
  bool success = bank0.init(rightKey, hw, sizeof(TestRecord));
  DASSERT(!success);

  // 2. wrong chip size
  Serial << F("wrong chip size...") << ENDL;
  h.recordSize = sizeof(TestRecord);
  h.chipLastAddress--;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &h, sizeof(h)));
  EEPROM_BankWithTools bank1(1);
  success = bank1.init(rightKey, hw, sizeof(TestRecord));
  DASSERT(!success);

  // 3. Wrong number of chips
  Serial << F("wrong number of chips...") << ENDL;
  h.recordSize = sizeof(TestRecord);
  h.chipLastAddress = hw.getExternalEEPROM_LastAddress(0);
  h.numChips++;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &h, sizeof(h)));
  EEPROM_BankWithTools bank2(1);
  success = bank2.init(rightKey, hw, sizeof(TestRecord));
  DASSERT(!success);

  Serial << ENDL;
}
#endif

#ifdef TEST_PERFORMANCE
void test_Performance() {
  Serial << ENDL << F("### Test 6: Performance") << ENDL;
  HardwareScanner hw;
  hw.init(10, 100);
  hw.printFullDiagnostic(Serial);
    // Clear data left from previous tests.
  DASSERT(hw.getNumExternalEEPROM() >= 2);
  unsigned int wrongKey = 0xBBAF;
  unsigned int rightKey = 0xABAF;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &wrongKey, sizeof(rightKey)));
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(2), 0, (byte *) &wrongKey, sizeof(rightKey)));
  EEPROM_Bank::EEPROM_Header h;
  h.headerKey = rightKey;
  h.chipLastAddress = hw.getExternalEEPROM_LastAddress(0);
  h.numChips = hw.getNumExternalEEPROM();
  h.recordSize = 60;
  h.firstFreeChip = 1;
  h.firstFreeByte = h.chipLastAddress - 2 - h.recordSize; // Chip 2 should accept one more record, not 2.
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(0), 0, (byte *) &h, sizeof(h)));
  EEPROM_BankWithTools bank0(1);
  bool success = bank0.init(rightKey, hw,60);
  DASSERT(success);
  bank0.printHeaderFromChip();
  
  Serial << F("Store 100 records of 60 bytes each, accross 2 chips, and check successful result...") << ENDL;
  char data[60];
  elapsedMillis writeTime=0;
  for (int i = 0; i<100;i++) {
    success = bank0.storeOneRecord(data, 60 );
    DASSERT(success);
  }
  unsigned long duration = writeTime;
  Serial << "Duration is " << writeTime << " msec, i.e. " << float (writeTime) / 100 << " msec per write." << ENDL; 

  Serial << ENDL;
}

#endif

#ifdef TEST_UNUSED
void test6_FirstChipNotUsed() {
  Serial << ENDL << F("### Test 6: first chip not used") << ENDL;
  HardwareScanner hw;
  hw.init(10, 100);
  hw.printFullDiagnostic(Serial);
  // Install header in EEPROM 2 & 3.
  DASSERT(hw.getNumExternalEEPROM() >= 2);
  unsigned int wrongKey = 0xBAAF;
  unsigned int rightKey = 0xAAAF;
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(0), 0, (byte *) &wrongKey, sizeof(wrongKey)));
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(2), 0, (byte *) &wrongKey, sizeof(wrongKey)));
  EEPROM_Bank::EEPROM_Header h;
  h.headerKey = rightKey;
  h.chipLastAddress = hw.getExternalEEPROM_LastAddress(0);
  h.numChips = hw.getNumExternalEEPROM() - 1;
  h.firstFreeChip = 0;
  h.firstFreeByte = sizeof(h);
  h.recordSize = sizeof(TestRecord);
  DASSERT(ExtEEPROM::writeData(hw.getExternalEEPROM_I2C_Address(1), 0, (byte *) &h, sizeof(h)));

  EEPROM_BankWithTools bank(1);
  bool success = bank.init(rightKey, hw, sizeof(TestRecord));
  DASSERT(success);

  bank.printHeaderFromChip();
  // The header should be found and used.
  unsigned long expectedSize = getExpectedTotalSize(hw) - hw.getExternalEEPROM_LastAddress(0) - 1;
  unsigned long expectedFreeSpace = expectedSize - sizeof(EEPROM_Bank::EEPROM_Header);
  checkSpace(bank, expectedSize, expectedFreeSpace);
  initTestData(0xbb, 0xbb);
  DASSERT(bank.leftToRead() == 0);
  DASSERT(bank.recordsLeftToRead() == 0);

  storeDataRecords(&bank, 1);
  expectedFreeSpace -= (sizeof(testData));
  checkSpace(bank, expectedSize, expectedFreeSpace);
  Serial << F("   left to read:") << bank.leftToRead() << ENDL;
  DASSERT(bank.leftToRead() == 1 * sizeof(TestRecord));
  DASSERT(bank.recordsLeftToRead() == 1);
  DASSERT(!bank.readerAtEnd());

  Serial << F("Read the 1 record with new EEPROM_Bank") << ENDL;
  // NB: do this AFTER first one updated header
  doIdle(bank, 2);
  EEPROM_BankWithTools bank1(1);
  success = bank1.init(rightKey, hw, sizeof(TestRecord));
  DASSERT(success);
  bank1.printHeaderFromChip();
  Serial << F("   left:") << bank1.leftToRead() << ENDL;
  Serial.flush();
  DASSERT(bank1.leftToRead() ==  sizeof(TestRecord));
  DASSERT(bank1.recordsLeftToRead() == 1);
  DASSERT(!bank1.readerAtEnd());
  readDataRecords(&bank1, 1);
  Serial << F("   left:") << bank1.leftToRead() << ENDL;
  DASSERT(bank1.leftToRead() == 0);
  DASSERT(bank1.recordsLeftToRead() == 0);
  DASSERT(bank1.readerAtEnd());

  Serial << F("Check 3 chips are used after erasing...") << ENDL ;
  bank1.erase();
  bank1.printHeaderFromChip();
  expectedSize = getExpectedTotalSize(hw);
  expectedFreeSpace = expectedSize - sizeof(EEPROM_Bank::EEPROM_Header);
  checkSpace(bank1, expectedSize, expectedFreeSpace);

  Serial << ENDL;
}
#endif
void setup()
{
  DINIT(19200);

  Serial << F("Starting tests") << ENDL;

#ifdef TESTS_0_TO_3
  //test0_NoEEPROM_Chip();
  test1_EmptyEEPROM();
  test2_EEPROM_NonEmptyWithHeader();
  test3_EEPROM_EmptyWithHeader();
  Serial << F("Tests 1 to 3 OK (run tests 4 to 6!)") << ENDL;
#endif

#ifdef TESTS_4_TO_6
  test4_EEPROM_MoreThanOneChipFull();
  test4b_WriteThenReadAccrossChips(); // Uncomment when ready!
  //test5_TwoHeaders(); // Do not use this test: it covers features which are not required.
  test5b_InconsistentHeaders();
  //test6_FirstChipNotUsed(); // Do not use this test: it covers features which are not required.
  Serial << F("Tests 4 to 6 OK") << ENDL;
#endif

#ifdef TEST_PERFORMANCE
  test_Performance();
#endif

  Serial << ENDL << F("Size of EEPROM_Bank: ") << sizeof(EEPROM_Bank) << ENDL;
  Serial << F("Size of TestRecord: ") << sizeof(TestRecord) << ENDL;

}

void loop() {
  delay(500);
}

