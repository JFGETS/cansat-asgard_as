/*
 * A Fake harware scanner which always reports 0 EEPROM chip
 */

 #pragma once
 #include "HardwareScanner.h"


 class TestHW_ScannerSmallChips : public HardwareScanner {
   VIRTUAL unsigned int getExternalEEPROM_LastAddress(const byte /* EEPROM_Number */ ) const { return 223 ;  /*This number is prime */ }
 };

