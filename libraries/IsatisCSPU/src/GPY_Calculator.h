#pragma once 
#include "GPY.h"
class GPY_Calculator: public GPY  {
  public:
   static double getDustDensity(double outputV, bool qualityOK);
   static double getAQI(double ugm3);
   static double Voc;
  protected:
#ifdef GPY_INCREASE_VOC 
   static double VoutReference;
#endif
   static byte VoutReferenceCount;
   static void adjustVoc(double outputV);
   static double computeDustDensity(double deltaV); 
};

