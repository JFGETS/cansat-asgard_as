#pragma once           //To include all of this hearders only once
#include "Arduino.h"   //Include this headers to use a specific type of variable

class BlinkingLed {
  public:           //All that's written in public can be changed by anyone
    
    //Declaration of the constructor and the run() method
    BlinkingLed(byte pinNumber, uint8_t duration);
     
    void run();

  private:          //All that's writen in private can't be changed by anyone

    //Declaration of the variables
    uint8_t duration;
    byte pinNumber;
    unsigned long ts = 0;
};
