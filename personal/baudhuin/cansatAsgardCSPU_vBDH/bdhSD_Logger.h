/*
   SD_Logger.h

   A class providing the required SD Services to the StorageManager

*/
#pragma once
#include <SdFat.h>
#include "elapsedMillis.h"

#define CLOSE_AFTER_EACH_ACCESS // Define  to have the file reopened and closed at each access.
 
class SD_Logger {
  public:
    const unsigned long maintenancePeriod = 10000; // maintenance period in milliseconds. 
    SD_Logger();
    /*  Initialize the SD library, checks if the required amount of free space is available,
        define the name of the log file and creates it.
        The filename is composed as AAAAnnnn.txt where:
          AAAA is the first 4 characters of the fourCharPrefix converted to uppercases,
          nnnn is the smallest non null integer number (on 4 digits) such as AAAAnnnn.txt does not exist.
        If msg is not an empty string, it is written as first line of the file.
        The file is then closed.
        Return 0 in case of error.
               -1: if initialization could not be performed: no card, card not formatted, card full, card read-only...
               1: if initialisation was succesfull but required free space is not available.
        Multiple inits should be supported (the last one overriding the previous one. */
    char init( const char * fourCharPrefix,
               const byte chipSelectPinNumber = 10,
               const String& msg = "",
               const unsigned int requiredFreeMegs = 1);

    /* Append data to the logfile, completed with a "\n" and flush the file */
    // TBD: Should we close the file after each write and reopen it?  To be evaluated with a performance test.
    bool log(const String& data);

    /* Return the name of the log file, if any, or an empty string if none. */
    const String& fileName()  const ;

    /*  This method should be called regularly in the loop() processing, to allow for internal maintenance
        (flushing the file to avoid data loss, etc.). After a few seconds, it should always be safe to unplug
        the board and remove the card. 
        Returns true if maintenance was actually performed. */
    bool doIdle();

    /*  Return the size in bytes of the logfile in bytes (does not work for files larger than 2Gb due to
     *  unsigned long limitation.
     */
    unsigned long fileSize() ;

    /* Return the free space on the SD card. 
     * freeSpaceInBytes() returns UINT_MAX if the free space is larger than UINT_MAX bytes.
     */
    unsigned long freeSpaceInMBytes() ;
    unsigned long freeSpaceInBytes() ;

  private:
    //Build a valid file name based on prefix and sequence number. 
    void buildFileName( const char* fourCharPrefix, const byte number);

    String logFileName;  // the name of the log file.
    SdFat SD;
#ifndef CLOSE_AFTER_EACH_ACCESS
    File logFile;
#endif
    elapsedMillis timeSinceMaintenance;
};

