
#include "BlinkingLED.h"


BlinkingLED::BlinkingLED(byte thePinNumber, unsigned long theDuration) {
   duration=theDuration;
   pinNumber=thePinNumber;
   pinMode(pinNumber, OUTPUT);
   elapsed=0;
}

void BlinkingLED::run() {
    if (elapsed >= duration) {
        digitalWrite(pinNumber, !digitalRead(pinNumber));
        elapsed=0;
    }
}
