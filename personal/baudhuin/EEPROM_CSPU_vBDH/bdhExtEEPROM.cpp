/*
 * ExtEEPROM.cpp (BDH version)
 * 
 * TODO Possible optimization: read value and write only if different. This is time-consuming and     
 * is possibly overkill...
 */

#include "ExtEEPROM.h"
//#define DEBUG
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#define DBG_WIRE_ERRORS 1
#define DBG_PAGE_WRITE 0
#define DBG_READ_DATA 0

#include <Wire.h>

#define USE_SINGLE_WRITE_FOR_BUFFER 
// if defined, the Wire.write(data, dataSize) function
// is used, instead of looping over the bytes.

bool ExtEEPROM::writeByte(const byte I2C_Address, const unsigned int address, const byte data)
{
  Wire.beginTransmission(I2C_Address);
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  Wire.write(data);
  byte status = Wire.endTransmission();
  if (status != 0) {
    DPRINTS(DBG_WIRE_ERRORS, "*** writeByte: error:");
    DPRINTLN(DBG_WIRE_ERRORS, status);
    return false;
  }
  else delay(5);
  return true;
}


bool ExtEEPROM::readByte( const byte I2C_Address, const unsigned int address, byte &data )
{
  Wire.beginTransmission(I2C_Address);
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  byte status = Wire.endTransmission();
  if (status != 0) {
    DPRINTS(DBG_WIRE_ERRORS, "*** readByte: error:");
    DPRINTLN(DBG_WIRE_ERRORS, status);
    return false;
  }
  Wire.requestFrom(I2C_Address, (byte) 1);

  if (Wire.available()) data = Wire.read();

  return true;
}


//TODO: Can we increase from 16 to 30 bytes per write? (use a constant!)
byte ExtEEPROM::writeData(  const byte I2C_Address,
                            const unsigned int startAddress,
                            const byte*buffer, const byte bufferSize) {
  // Uses Page Write for 24LC256. Allows for 64 byte page boundary, Splits data into max 16 byte writes
  // Code based on http://www.hobbytronics.co.uk/eeprom-page-write, generalised to support any type of data.
  unsigned char byteCounter = 0;
  unsigned int  address;
  unsigned int  page_space;
  unsigned int  page = 0;
  unsigned int  num_writes;
  unsigned char first_write_size;
  unsigned char last_write_size;
  unsigned char write_size;

  DPRINTS(DBG_PAGE_WRITE, "buffer size: ");
  DPRINTLN(DBG_PAGE_WRITE, bufferSize);
  
  // Calculate space available in first page
  page_space = int(((startAddress / 64) + 1) * 64) - startAddress;

  // Calculate first write size
  if (page_space > 16) {
    first_write_size = (page_space % 16);  // AB: Changed
    if (first_write_size == 0) first_write_size = 16;
  }
  else {
    first_write_size = page_space;
  }
  if (first_write_size > bufferSize) {
    first_write_size = bufferSize; //AB: added. Didn't work for dataSize < 16. 
  }

  // calculate size of last write
  if (bufferSize > first_write_size)
    last_write_size = (bufferSize - first_write_size) % 16;
  else
    last_write_size = 0; // Added AB.

  // Calculate how many writes we need
  if (bufferSize > first_write_size)
    num_writes = ((bufferSize - first_write_size) / 16) + 2;
  else
    num_writes = 1;

  DPRINTS(DBG_PAGE_WRITE, "page space (bytes) : ");
  DPRINTLN(DBG_PAGE_WRITE, page_space);
  DPRINTS(DBG_PAGE_WRITE, "first write: ");
  DPRINT(DBG_PAGE_WRITE, first_write_size);
  DPRINTS(DBG_PAGE_WRITE, " bytes, last write: ");
  DPRINT(DBG_PAGE_WRITE, last_write_size);
  DPRINTS(DBG_PAGE_WRITE, " bytes, num writes: ");
  DPRINTLN(DBG_PAGE_WRITE, num_writes);

  byteCounter = 0;
  address = startAddress;
  for (page = 0; page < num_writes; page++)
  {
    if (page == 0) write_size = first_write_size;
    else if (page == (num_writes - 1)) write_size = last_write_size;
    else write_size = 16;

    Wire.beginTransmission(I2C_Address);
    Wire.write((int)((address) >> 8));   // MSB
    Wire.write((int)((address) & 0xFF)); // LSB
#ifndef USE_SINGLE_WRITE_FOR_BUFFER
    for (byte counter = 0; counter < write_size; counter++) {
      DASSERT(byteCounter < bufferSize);
      Wire.write((byte) buffer[byteCounter]);
      byteCounter++;
    }  // AB: CHANGED: we do not work with null-terminated strings.
#else
    DPRINTS(DBG_PAGE_WRITE, "byteCounter/write_size:");
    DPRINT(DBG_PAGE_WRITE, byteCounter);
    DPRINTS(DBG_PAGE_WRITE, "/");
    DPRINT(DBG_PAGE_WRITE, write_size);
    DPRINTS(DBG_PAGE_WRITE, " bufferSize: ");
    DPRINTLN(DBG_PAGE_WRITE, bufferSize);

    DASSERT(((byteCounter + write_size) <= bufferSize));
    Wire.write( &buffer[byteCounter], write_size);
    byteCounter += write_size;
#endif

    byte status = Wire.endTransmission(); // Changed
    if (status != 0) {
      DPRINTS(DBG_WIRE_ERRORS, "*** writeData: error:");
      DPRINTLN(DBG_WIRE_ERRORS, status);
      return byteCounter - write_size; // The number of bytes transmitted successfully.
    }
    address += write_size; // Increment address for next write

    delay(6);  // needs 5ms for page write
  }
  return (address - startAddress);
}

byte ExtEEPROM::readData(   const byte I2C_Address,
                            const unsigned int address,
                            byte*buffer, const byte bufferSize ) {
  unsigned char i = 0;
  DPRINTS(DBG_READ_DATA, "I2C 0x");
  DPRINT(DBG_READ_DATA, I2C_Address, HEX);
  DPRINTS(DBG_READ_DATA, ", address: 0x");
  DPRINT(DBG_READ_DATA, address, HEX);
  DPRINTS(DBG_READ_DATA, ", buffer size: ");
  DPRINTLN(DBG_READ_DATA, bufferSize);

  Wire.beginTransmission(I2C_Address);
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  byte status = Wire.endTransmission();
  if (status != 0) {
    DPRINTS(DBG_WIRE_ERRORS, "*** readData: error:");
    DPRINTLN(DBG_WIRE_ERRORS, status);
    return 0;
  }
  Wire.requestFrom(I2C_Address, bufferSize);
  while (Wire.available()) buffer[i++] = Wire.read();

  return i;
}




