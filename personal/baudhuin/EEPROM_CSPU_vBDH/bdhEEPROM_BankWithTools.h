/*
 * This is a subclass of EEPROM_Bank completed with utility methods for testing only
 */

#pragma once
#include "EEPROM_Bank.h"

class EEPROM_BankWithTools : public EEPROM_Bank {
public:
	EEPROM_BankWithTools(const unsigned int theMaintenancePeriodInSec);

	/* Utility method: write memory content in user readable format */
	VIRTUAL_FOR_TEST void printMemory() const;
	/* Utility method: dump memory in hex format */
	VIRTUAL_FOR_TEST void hexDumpData() const;
	/* Utility method: read header from chip */
	VIRTUAL_FOR_TEST void printHeaderFromChip() const;

private:
	VIRTUAL_FOR_TEST void hexDumpBuffer(const byte * buffer, const unsigned int bufferSize) const;
};
