#include "EnumSOS.h"                                             //Includes the header file "EnumSOS.h"

EnumSOS::EnumSOS (byte thePinNumber, uint16_t thePauseTime) { //Declares the constructor for the class "EnumSOS" with parameters for the pin the Sos will be ran on, and the time that will elapse between sos'es
  pinNumber = thePinNumber;                                         //Transfer the value of public "thePinNumber" over to private "pinNumber"
  pauseTime = thePauseTime;                                         //Transfer the value of public "thePauseTime" over to private "pauseTime"
  ts = millis();                                                    //Saves the value of millis in a time stamp
  state = SosState::BipS;                                           //Sets the state ov the variable ledState to the enum BipS
  pinMode(pinNumber, OUTPUT);                                       //Sets the pinMode of the chosen pin to OUTPUT
}
void EnumSOS::ThreeBeeps (uint16_t sosInterval,SosState nextState){
        if ((millis() - ts) >= sosInterval) {                       //If statement to check if 250miliseconds has elapsed
        digitalWrite(pinNumber, !digitalRead(pinNumber));           //Writes the oposite of the valut of pinNumber to pinNumber
        ts = millis();                                              //Saves the current value of millis() to the Time Stamp
        counter++;                                                  //Adds one to "counter"
        if (counter >= 6 ) {                                        //If statement that checks if counter is greater or equal than 6
          state = nextState;                                        //Sets the value of ledState to BipO for the switch to run the BipO Case
          counter = 0;                                              //resets the counter
        }
      }
  }
void EnumSOS::run() {                                            //Declares the run() method
  switch (state) {                                                  //Declares a switch statement that triggers the case that "ledState" is equal to
    case SosState::BipS :                                           //Declares the 1st case that runs three short blinks and three short blinks at the end of the sos
      ThreeBeeps(250, SosState::BipO);
      
      break;                                                        //Used to prevent the code to continue on the BipO case

    case SosState::BipO :                                           //Declares the 2nd case that runs three long blinks
      ThreeBeeps (500, SosState::SndBipS);
      
      break;                                                        //Used to prevent the code to continue onto case PauseState

    case SosState::SndBipS :
      ThreeBeeps (250, SosState::PauseState);
      
      break;

    case SosState::PauseState :                                     //Declares the 3rd case that runs a pause in the sos of time value "pauseTime"
      if ((millis() - ts) >= pauseTime) {                           //If statement to check if the inputted time has elapsed
        ts = millis();                                              //saves the current time
        state = SosState::BipS;
        counter = 0;                                                //Resets the "Counter Variable"
      }
      
      break;                                                        //Used to prevent the code to continue onto case default

    default :
      Serial.println ("YET UNKNOWN ERROR OCCURED");
      
      break;                                                        //Used to prevent the code to continue onto case bipS
  }
}
