/*
 * Simple utility program sending numbered strings on the Serial interface.
 * Used for testing the transmission.
 */


constexpr int ledPin =  LED_BUILTIN;

uint16_t counter=0;

void setup() {
  Serial.begin(115200);
  while (!Serial) delay(10);
  Serial.println("Setup ok");
  pinMode(ledPin, OUTPUT); 
}

void loop() {
  Serial.print(counter++);
  Serial.println(F(": A dummy string"));
  digitalWrite(ledPin, !digitalRead(ledPin));
  delay(1000);
}
