#include "IsatisConfig.h"
#include "GPY_Calculator.h"

const bool PrintToSerial = false;

//********** Constants defining the content of the files. ********
const bool shortVersion = false;                // If true, the length of the pre-flight and post-landing parts
                                                // are reduced to 5 seconds.
const bool addJitter = true;                   // If true, noise is added to all data (see settings below).

// All durations in msec
const unsigned long LoggingStartTime = 2034;    // Time of first measurement logged.
const unsigned long DurationUntilLaunch = (shortVersion ? 5000L : 10L * 60L * 1000L); // Time between power-up and ignition.
const unsigned long DurationOfAscent = 9500;    // The duration of the ascent to max altitude (msec).
const unsigned long DurationOfMeasurement = 85; // Duration of each reading in msec.
const unsigned long DurationAfterLanding = (shortVersion ? 5000L : 5L * 60 * 1000L); // The time after the can landed.
const float MaxDeltaAltitude = 1059.2;          // maximum altitude reached (in m)
const float BaseAltitude = 93.4;                // Altitude of launch pad (in m)
const float GroundLevelPressureIn_hPa = 1002.1;
const float GroundLevelTemperature = 19.0;      // °C
const float DescentSpeed = 10.2;                // m/sec

// Max jitter values (added or substracted from actual values).
const float MaxAltitudeJitter = (addJitter ? 2.0 : 0.0);            // Maximum jitter on calculation of altitude from time.
const unsigned long MaxTimeJitter = (addJitter ? 10 : 0);           // msec
const unsigned long maxDurationJitter = (addJitter ? 10 : 0);       // msec
const float MaxPressureJitterIn_hPa = (addJitter ? 2.0 : 0.0);      // hPa
const float MaxTemperatureJitter = (addJitter ? 1.0 : 0.0);         // °C
const float MaxOutputV_Jitter = (addJitter ? GPY_MinSignificantDeltaV : 0.0);     // V

const float rateOfExcessivelyLongMeasures = 0.05;
const int   ExtraDurationForLongMeasures = 150; // msec

const float MinGPY_DustDensity = 0.0; // density at apex
const float MaxGPY_DustDensity = 18.3; // dust density at ground.
const float RateOfBadQualityGPY_Readings = 0.2;

// Stable constants
const float GravitationalConstant = 9.81;

// Calculated constants
const float MaxAltitude = BaseAltitude + MaxDeltaAltitude;
const float AscentSpeed = ((float)MaxDeltaAltitude) / ((float)DurationOfAscent) ;
const unsigned long GroundLevelPressureInPa = 100.0 * GroundLevelPressureIn_hPa;
const float GroundLevelTemperatureInK = GroundLevelTemperature + 273.15;
const unsigned long DurationUntilDescent = DurationUntilLaunch + DurationOfAscent;
const unsigned long DurationOfDescent = (unsigned long) (MaxDeltaAltitude / DescentSpeed * 1000.0);
const unsigned long DurationUntilLanding = DurationUntilDescent + DurationOfDescent;
const unsigned long DurationUntilPowerDown = DurationUntilLanding + DurationAfterLanding;

const float MinGPY_OutputV = GPY_Calculator::Voc + MinGPY_DustDensity / (float) GPY_SensitivityInMicroGramPerV;
const float MaxGPY_OutputV = GPY_Calculator::Voc + MaxGPY_DustDensity / (float) GPY_SensitivityInMicroGramPerV;
const float GPY_DeltaOutputV = MaxGPY_OutputV - MinGPY_OutputV;

//************************************

